cd ../exercise
composer install
php artisan key:generate
php artisan cache:clear
php artisan config:clear
php artisan config:cache
php artisan view:clear
php artisan migrate
php artisan import:superhero-db

php artisan serve --port=8010

echo 'corriendo en puerto 8010'
read