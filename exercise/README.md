
## Exercise
We will divide the exercise in 3 parts:

1. **Data modeling:**
Define a correct structure to import the `CSV` and manage the database efficiently based on eloquent.

2. **Data import:**
Create a script to import `csv/superheros.csv` in the models database tables. No MySQL have to be provided, the script need to allow us to import the data correctly.

3. **API to retrieve data:**
Generate a API endpoint to retrieve superheroes. The endpoint need to be able to filter by GET params (three properties at least), order and paginate the results.

## Setup and Config

### Requirements
1. PHP
2. MySQL database
3. Composer


### Installation guide

1. Clone the repo:`git clone git@bitbucket.org:hmpetrone/exercice-laravel-resolved.git`
2. Once you cloned the repository, you will need to create a MySQL database
3. Setup the `exercise/.env` file (Example .env.example)
4. Execute the command: `sh scripts/superheroesScript.sh`
5. You are ready to test Api (Example `http://127.0.0.1:8010/api/list_superheros?power=10&race=human&orderBy=power&page=1`).

### Usage step 5

1. Cualquier propiedad que esté en el modelo como parámetro GET.
2. Por defecto usa 'orderBy' con 'id', pero podés usar cualquier propiedad del modelo (Siempre ordena ASC).
3. Con el parámetro 'page' podés indicar que página queres. Si no se declara trae la página 1 por defecto.
4. Con el parámetro 'cantPerPage' podés indicar la cantidad de resultados por página. Si no se declara trae 10 resultados por defecto