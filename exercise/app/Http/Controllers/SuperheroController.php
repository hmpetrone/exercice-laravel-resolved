<?php

namespace App\Http\Controllers;

use App\Superhero;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class SuperheroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list(Request $request)
    {
        //array que uso para comprobar que los parametros sean validos
        $validos=[
            'id' => '=',
            'name'=> 'LIKE',
            'fullName'=> 'LIKE',
            'strength'=> '=',
            'speed'=> '=',
            'durability'=> '=',
            'power'=> '=',
            'combat'=> '=',
            'race'=> 'LIKE',
            'height/0'=> 'LIKE',
            'height/1'=> 'LIKE',
            'weight/0'=> 'LIKE',
            'weight/1'=> 'LIKE',
            'eyeColor'=> 'LIKE',
            'hairColor'=> 'LIKE',
            'publisher'=> 'LIKE',
            'page'=>1,
            'cantPerPage'=>10,
            'orderBy'=>'id',
        ];

        //inicializo la query
        $superheroes = DB::table('superhero');
        //recorro los parametros
        foreach ($request->request as $key => $value) {

            //compruebo que los parametros recibidos sean los esperados
            if(!in_array($key,array_keys($validos))){
                return response()->json([
                    'error' => "El parámetro '".$key."' inválido.",
                    'expectedParams' => array_keys($validos)
                ]);
            }
            //si no es un atributo del modelo salteo la iteracion
            if(in_array($key,['page','cantPerPage','orderBy'])){

                //controlo que el valor del parametro orderBy sea valido
                if ($key == 'orderBy'){
                    if(!in_array($value,array_keys($validos))){
                        return response()->json([
                            'error' => "El valor del parametro ".$key." no existe en el modelo"
                        ]);
                    }
                }
                //piso los valores por default de page, cantPerPage o orderBy
                $validos[$key] = $value;
            }else{
                if($validos[$key] == 'LIKE'){
                    $superheroes->where($key,$validos[$key],'%'.$value.'%');
                }else{
                    $superheroes->where($key,$validos[$key],$value);
                }
            }
        }

        $superheroes->orderBy($validos['orderBy'], 'ASC');
        $result = $superheroes->paginate($validos['cantPerPage']);

        //controlo que tenga la pagina buscada
        if($validos['page'] > $result->lastPage()){
            return response()->json(['error' => 'Error: paginas encontradas '.$result->lastPage().'.']);
        }

        return response()->json([
            'total' => $result->total(),
            'items' => $result->items(),
            'page' => $result->currentPage(),
            'cantPages' => $result->lastPage(),
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Superhero  $superhero
     * @return \Illuminate\Http\Response
     */
    public function show(Superhero $superhero)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Superhero  $superhero
     * @return \Illuminate\Http\Response
     */
    public function edit(Superhero $superhero)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Superhero  $superhero
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Superhero $superhero)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Superhero  $superhero
     * @return \Illuminate\Http\Response
     */
    public function destroy(Superhero $superhero)
    {
        //
    }
}
