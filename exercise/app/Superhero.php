<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Superhero extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'superhero';

    //
    protected $fillable = [
        'id',
        'name',
        'fullName',
        'strength',
        'speed',
        'durability',
        'power',
        'combat',
        'race',
        'height/0',
        'height/1',
        'weight/0',
        'weight/1',
        'eyeColor',
        'hairColor',
        'publisher',
    ];
}
