<?php

namespace App\Console\Commands;

use App\Superhero;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class importSuperheroData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:superhero-db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Superheroes data from static .csv to the database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        echo 'Importing...     ';
        $filepath = base_path("..\csv\superheros.csv");
        // Reading file
        $file = fopen($filepath, "r");
        $importData_arr = array(); // Read through the file and store the contents as an array
        $i = 0;
        //Read the contents of the uploaded file
        while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
            $num = count($filedata);
            // Skip first row (Remove below comment if you want to skip the first row)
            if ($i == 0) {
                $i++;
                continue;
            }
            for ($c = 0; $c < $num; $c++) {
                $importData_arr[$i][] = $filedata[$c];
            }
            $i++;
        }
        fclose($file); //Close after reading
        foreach ($importData_arr as $importData) {

            try {
                DB::beginTransaction();
                Superhero::create([
                    'id' => $importData[0],
                    'name' => $importData[1],
                    'fullName' => $importData[2],
                    'strength' => $importData[3],
                    'speed' => $importData[4],
                    'durability' => $importData[5],
                    'power' => $importData[6],
                    'combat' => $importData[7],
                    'race' => $importData[8],
                    'height/0' => $importData[9],
                    'height/1' => $importData[10],
                    'weight/0' => $importData[11],
                    'weight/1' => $importData[12],
                    'eyeColor' => $importData[13],
                    'hairColor' => $importData[14],
                    'publisher' => $importData[15],
                ]);
                DB::commit();
            } catch (\Exception $e) {
                //throw $th;
                echo('Import has failed =( ');
                DB::rollBack();
                return 1;
            }
        }
        echo 'Successfully imported!';
        return 0;
    }
}
